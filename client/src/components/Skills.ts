import React from 'react';
import * as I from '../interfaces';
interface skill {
    title: string;
    description: string;
    language: string;
}
export async function getPage(): Promise<I.Page> {
    //data 
    const skillList: skill[] = [{
        title: "Full Stack Node.js",
        description: "based Web Development",
        language: "React Front End, Express Backend, MySQL Database",
    }, {
        title: "Object Oriented Paradigm",
        description: "Software Patterns and Architecture",
        language: "C++, Java",
    }, {
        title: "Operating Systems Development",
        description: "Machine level interaction for Internet of Things devices",
        language: "Bash, C, C++, Arduino, MBed",

    }, {
        title: "Runtime Analysis and Optimization",
        description: "Big-O Analysis and understanding of Time Complexity",
        language: "Psuedocode, Algorithm Notation",
    },];

    //configure page 
    let page: I.Page = new I.Page("Skills");
    page.data = skillList.map(({ title, description, language }) => {
        let card = new I.Card();
        card.title = `${title} | ${language}`;
        card.body = description;
        return card;
    });
    //return new Promise(()=>{return page});
    return page;
}