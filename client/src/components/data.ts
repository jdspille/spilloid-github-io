import * as Skills from './Skills';
import * as Experience from './Experience';
import * as Portfolio from './Portfolio';
import * as AboutMe from './AboutMe';
import { Page } from '../interfaces';


export async function getData() : Promise<Page[]>{
    return ([
        await AboutMe.getPage(),
        await Skills.getPage(),
        await Experience.getPage(),
        await Portfolio.getPage(),
    ]);
}