
import React from 'react';
import * as I from '../interfaces';

export async function getPage(): Promise<I.Page> {
    //get latest repos
    const response = await fetch('https://api.github.com/users/spilloid/repos');
    const data = await response.json();
    //configure page 
    let page = new I.Page("Portfolio");
    //stuff fetch result into clean cards
    //TODO: Fix dirty solution of 'any' use
    page.data = data.map(({ name, description, html_url, language } : any ) => {
        let card = new I.Card();
        card.title = `${name} | ${language}`;
        card.body = description;
        card.url = html_url;
        return card;
    });
    return page;
}
