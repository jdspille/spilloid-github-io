import React from 'react';
import '../navbar.css';
import * as I from '../interfaces';
const Navbar = (props : I.navBarProps) =>{
    return(<div className="nav">
                <nav>
                    <ul>
                        {props.cards.map((card,id)=>{
                            return(<li><a onClick={(e)=>{props.clicked(id)}}>{card}</a></li>);
                            
                        })}
                    </ul>
                </nav>
        </div>);
}
export default Navbar;