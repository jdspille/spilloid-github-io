import * as I from '../interfaces';
export async function getPage() : Promise<I.Page>{
    let page = new I.Page("About Me");
    let aboutMe = new I.Card("About Me");
    aboutMe.body = "When a front end needs to load quickly, and provide a snappy experience that leaves the user informed and comfortable. A back end needs to be clean and secure, providing scalable I/O and broad portability. I'm a GNU/Linux enthusiast who believes in the necessity for intuitive algorithms and utilization of software patterns. I think twice and code once, with UML diagrams and continuous documentation covering each step in the way. I discovered the beauty of JavaScript after learning the underlying mechanisms of Apache servers and REST API's. React is FAST! So long as you keep components at the forefront, the User Experience will be excellent. Express is clean, providing only the cornerstone needed to build an infrastructure, meaning I can write exactly what your company needs without all the bloat from other back ends. Websites are easier to use than they've ever been before, so as your developer I'll do what I did for the lawyers I used to work with: cut out the complexity and ask questions that get at the root of what you want. I use continuous integration standards and plain English. Each feature I introduce will be explained and documented so you can deliver the exact experience you want to your customers.";
    page.data = [aboutMe]; 
    //return new Promise(()=>{return page});
    return page;
}