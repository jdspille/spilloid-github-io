import * as I from '../interfaces';
export async function getPage() : Promise<I.Page>{
    let experience = [{
            title : "Full Stack Software Developer",
            date : "January 2018-July 2019",
            location : "IUPUI Graduate Psychology Department",
            description : "Created and maintained a collection of web interfaces which gather data from roughly 100 individuals with varying positions in the department. Website was constructed with HTML5/CSS/PHP/JavaScript/MySQL, however is now under a React/Express/MySQL Stack.",
    },{
            title : "Captain (Independent Contractor)",
            date : "September 2015-Present",
            location : "Bellhops Moving Company",
            description : "Lead A team of between 2 and 6 in aiding an individual move house. Utilized a 24’ truck with a lift gate, maintained positive customer relations, and abide by specifics of handbook.",
         
    },{
            title : "Computer Support Specialist",
            date : "May 2014-May 2019",
            location : "Law offices of Yvonne M Spillers, Nathan Hoggatt, and Jody Dietsch",
            description : "Ran a remote connection software (Tight VNC) to allow for general assistance of each standing attorney as well as their staff whenever IT issues occurred.",
    },{
            title : "Teacher's Assistant",
            date : "August 2017-December 2018",
            location : "IUPUI School of Science",
            description : 'Lead "Project Lead Team Learning" based classroom of thirty students in a recitation-style. Retroactively had to diagnose bugs in code in a timely fashion, and describe this issue to students, leading them to fix the line of thinking to avoid future bugs of that type, without explicitly stating where their errors existed.',
    },];
    let page = new I.Page("Experience");
    page.data = experience.map(({title,date,description,location}) => {
        let card = new I.Card();
        card.title = `${title} | ${location} | ${date}`;
        card.body = description;
        return card;
    });
    //return new Promise(()=>{return page});
    return page;
}