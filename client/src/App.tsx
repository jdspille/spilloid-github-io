import React from 'react';
import NavBar from './components/NavBar';
import Me from './components/Me';
import './index.css';
import * as I from './interfaces';
import {getData} from './components/data';
interface AppProps { };
interface AppState {
  pages: I.Page[],
  selected: number
};

class App extends React.Component<void, AppState>{
  constructor() {
    super();
    this.state = { pages: [], selected: -1 };
  }
  componentDidMount() {
    /**replace with fetch call */
    const book = getData();
    book.then(book => {
      this.setState({ pages: book, selected: 0 });
    });
  }
  render() {
    const page = this.state.pages[this.state.selected];
    const card = page?.data?.map(({body,title,url})=>{
      return <div>
        <a href={url}>{title}</a>
        <p>{body}</p>
      </div>
    })
    return (
      <div className="App">
        <div className="container">
          <div className="header">Joseph Spillers</div>
          <NavBar 
            cards={this.state.pages.map((page)=>{return page.name})} 
            clicked={(name)=>{this.setState({selected : name})}}/>
            <div className = "content">
          <h1>{page?.name}</h1>
          {card}
          </div>
            <Me />
        </div>
      </div>
    );
  }
}

export default App;
