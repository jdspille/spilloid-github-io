export class Card {
  title : string;
  body : string;
  url : string;
  constructor(title? : string, body? : string, url? : string){
    this.title = title===undefined?"":title;
    this.body = body===undefined?"":body;
    this.url = url===undefined?"":url;
  }
}
export class Page { 
  name : string;
  data : Card[] | undefined;
  constructor(name? : string, data? : Card[]){
    this.name = name==undefined?"":name;
    this.data = data;
  }
}

export interface navBarProps{
  cards : string[];
  clicked : (cardIndex : number)=>void;
}